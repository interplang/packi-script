# Packi Script
Packi is the package repository for the Interp scripting language.  
This script automates the installation, upgarding, and removing of Packi packages.

## Installation Script
This script installs the packi script.
The installer will prompt you to ask if [Packi+](https://packi.kriikkula.net/packiplus) should be installed. This is recommended.

### Linux & MacOS
Run the following command to install:
```sh
curl https://gitlab.com/interplang/packi-script/-/raw/main/install.it > /tmp/install.it && interp /tmp/install.it
```

### Windows
```bat
curl https://gitlab.com/interplang/packi-script/-/raw/main/install.it > %tmp%/install.it && interp %tmp%/install.it
```

## CLI Tools
CLI Tools are provided with the [Packi+ package](https://packi.kriikkula.net/packiplus)

## API Usage
You can call packi from interp code.

### Require a package
Add this to your project if you wish to use the version 1.0.0 of the *helloworld* package in your code.
```
imp "@packi/lib.it";
packi::Require("helloworld", "1.0.0");
imp "@helloworld-1.0.0/helloworld.it"
```

## Uninstall
Packi can be uninstalled like a any normal package.

## Manual Installation
1. Make sure *interplang* is installed.
2. Check the interp package directory with `interp --print-package-dir`.  
   This can be for example:  
   Linux: **/home/user/samuel/.local/share/interp**  
   Windows: **C:\\Users\\samuel\\AppData\\Roaming\\interp**  
   MacOS: **/Users/samuel/Library/Application Support/interp**  
3. Create a directory called **packi** inside the package directory.
4. Copy the appropriate script of your platform (ex. **macos.it** for MacOS) to **packi/lib.it**
